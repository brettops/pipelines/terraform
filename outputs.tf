output "secret" {
  value     = var.super_secret
  sensitive = true
}
