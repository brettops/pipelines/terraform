> **NOTE:** This project has been moved to [buildgarden/pipelines/helm](https://gitlab.com/buildgarden/pipelines/helm). This version remains here for backwards compatibility.

# Terraform Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/terraform/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/terraform/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Deploy Terraform-managed infrastructure from GitLab.

## Usage

Include the pipeline:

```yaml
include:
  - project: brettops/pipelines/terraform
    file: dual.yml
```

# Example Module Docs

<!-- prettier-ignore-start -->
<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [random_string.random](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_super_secret"></a> [super\_secret](#input\_super\_secret) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_secret"></a> [secret](#output\_secret) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->
