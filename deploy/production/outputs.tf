output "secret" {
  value     = module.root.secret
  sensitive = true
}
